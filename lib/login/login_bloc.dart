import 'package:bloc/bloc.dart';
import 'package:flutter_bloc_login/authentication/index.dart';
import 'package:flutter_bloc_login/login/index.dart';
import 'package:flutter_bloc_login/repository/user_repository.dart';
import 'package:meta/meta.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({@required this.userRepository, @required this.authenticationBloc})
      : assert(userRepository != null),
        assert(authenticationBloc != null);

  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(
      LoginState currentState, LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginIsLoading();

      try {
        final token = await userRepository.authenticate(
          username: event,
          password: event,
        );

        authenticationBloc.dispatch(LoggedIn(token: token));

        yield LoginInitial();
      } catch (e) {
        yield LoginFailure(error: e.toString());
      }
    }
  }
}
